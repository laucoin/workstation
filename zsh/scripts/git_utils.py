from os import path

def is_repository(logger, debug):
    logger.info('Checking if `pwd` is a repository')

    if (path.exists('.git') is False):
        message = f'"{path.dirname}" is not a repository'
        logger.error(message)
        if (debug is True):
            raise Exception(message)
        return False

    return True
