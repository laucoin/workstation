from subprocess import check_output, CalledProcessError
from re import search, match
from shutil import copytree, rmtree

def refresh_hooks(logger, force, config_dir, work_dir, debug):
    repositories = _get_repositories(logger, work_dir, debug)
    for path in repositories:
        _refresh_repository_hooks(logger, force, path, config_dir)

def _get_repositories(logger, work_dir, debug):
    try:
        logger.info(f'Searching for repositories in "{work_dir}"')
        command = ['find', work_dir]
        result = check_output(command)
        # 1 array element = 1 path
        repositories = result.decode().splitlines()
    except CalledProcessError as e:
        find_error_message = f'Failed to run `{" ".join(command)}`:\n{e}'
        logger.error(find_error_message)
        if (debug is True):
            raise CalledProcessError(find_error_message)
        exit (find_error_message)

    # Get only the .git/hooks path (1 unique path by repository)
    regex_pattern = '\\.git/hooks$'
    repositories = [s for s in repositories if search(regex_pattern, s)]

    # Crash if no repository
    if (len(repositories) == 0):
        no_repo_error_message = 'No repository found! You should try with another path'
        logger.error(no_repo_error_message)
        exit()

    logger.info(f'{str(len(repositories))} repositories found')
    return repositories



def _refresh_repository_hooks(logger, force, path, config_dir):
    name = match('\\/.*\\/(.*)\\/.git\\/hooks', path)[1]
    logger.info(f'Refreshing "{name}" hooks')

    try:
        copytree(config_dir + '/git/git-templates/hooks', path)
        logger.info(f'Hooks are up to date for "{name}"')
    except FileExistsError:
        if (force):
            logger.warning('Files already exist. Forcing...')
            rmtree(path)
            _refresh_repository_hooks(logger, force, path, config_dir)
        else:
            logger.warning(f'Skipping "{name}" because file already exist')
