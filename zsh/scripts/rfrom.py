from git_utils import is_repository
from subprocess import check_output, CalledProcessError

def refresh_from(logger, default, branch, debug):
    is_repository(logger, debug)
    branch = _get_branch(logger, branch, debug)

    if (branch == default):
        logger.warning(f'You ask for rebase "{branch}" on "{default}", these are the same... Just pulling')
        _pull_branch(logger, branch, debug)

    _switch_to_branch(logger, default, debug)
    _pull_branch(logger, default, debug)
    _switch_to_branch(logger, branch, debug)

    try:
        logger.info(f'Rebase "{branch}" on "{default}"')
        command = ['git', 'rebase', default]
        return check_output(command)
    except CalledProcessError as e:
        rebase_error_message = f'Failed to rebase `{" ".join(command)}`:\n{e}'
        logger.error(rebase_error_message)
        if (debug is True):
            raise CalledProcessError(rebase_error_message)
        exit()

def _pull_branch(logger, branch, debug):
    try:
        logger.info(f'Pulling branch "{branch}"')
        command = ['git', 'pull']
        check_output(command)
    except CalledProcessError as e:
        pull_error_message = f'Failed to pull `{" ".join(command)}`:\n{e}'
        logger.error(pull_error_message)
        if (debug is True):
            raise CalledProcessError(pull_error_message)
        exit()

def _get_branch(logger, branch, debug):
    if branch:
        _switch_to_branch(logger, branch, debug)
        return branch

    # If no branch specified, get current one
    try:
        logger.info('Getting current branch')
        command = ['git', 'branch', '--show-current']
        return check_output(command).decode().strip()
    except CalledProcessError as e:
        branch_error_message = f'Failed get current branch with `{" ".join(command)}`:\n{e}'
        logger.error(branch_error_message)
        if (debug is True):
            raise CalledProcessError(branch_error_message)
        exit()

def _switch_to_branch(logger, branch, debug):
    try:
        logger.info(f'Switching to "{branch}"')
        command = ['git', 'checkout', branch]
        check_output(command)
    except CalledProcessError as e:
        switch_error_message = f'Failed to switch branch `{" ".join(command)}`:\n{e}'
        logger.error(switch_error_message)
        if (debug is True):
            raise CalledProcessError(switch_error_message)
        exit()