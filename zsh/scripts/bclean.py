from git_utils import is_repository
from subprocess import check_output, CalledProcessError
from re import compile, IGNORECASE

def branches_clean(logger, default, branches, debug):
    is_repository(logger, debug)
    _switch_default(logger, default, debug)
    to_delete = _get_branches_to_delete(logger, branches, debug)

    if (not to_delete):
        already_clean_message = 'Repository is already clean'
        logger.warning(already_clean_message)
        return None

    logger.info(f'Branches to delete: {", ".join(to_delete)}')

    try:
        logger.info('Deleting branches')
        command = ['git', 'branch', '-D'] + to_delete
        return check_output(command)
    except CalledProcessError as e:
        delete_error_message = f'Failed to delete branches `{" ".join(command)}`:\n{e}'
        logger.error(delete_error_message)
        if (debug is True):
            raise CalledProcessError(delete_error_message)
        exit()

def _switch_default(logger, default, debug):
    try:
        logger.info(f'Switching to default branch "{default}"')
        command = ['git', 'checkout', default]
        return check_output(command)
    except CalledProcessError as e:
        pull_error_message = f'Failed to switch to the default branch `{" ".join(command)}`:\n{e}'
        logger.error(pull_error_message)
        if (debug is True):
            raise CalledProcessError(pull_error_message)
        exit()

def _get_branches_to_delete(logger, branches, debug):
    # Get all branches
    try:
        logger.info('Getting branches to delete')
        command = ['git', 'branch']
        existing_branches = check_output(command).decode().splitlines()
    except CalledProcessError as e:
        branches_error_message = f'Failed to list repository branches `{" ".join(command)}`:\n{e}'
        logger.error(branches_error_message)
        if (debug is True):
            raise CalledProcessError(branches_error_message)
        exit()

    # Exclude unwanted branches
    logger.info("|".join(branches))
    branches_pattern = compile(r'^(%s)$' % '|'.join(branches), IGNORECASE)
    asterisk_pattern = compile(r'^\*')
    return [branch.strip() for branch in existing_branches if not branches_pattern.match(branch.strip()) and not asterisk_pattern.match(branch.strip())]
