from click import group, option, argument
from mylogger import mylogger
from os import environ, path
from bclean import branches_clean
from rfrom import refresh_from
from rhooks import refresh_hooks

@group()
def git_ext():
    """This is a git cli extension"""
    pass

@git_ext.command(context_settings={'show_default': True})
@argument('branch', type=str, default=environ.get('GIT_DEFAULT_BRANCH'), required=False)
@option('-b', '--branches', type=str, multiple=True, default=['develop', 'main', 'master'], help='List of excluded branches (can be used multiple times)')
@option('--debug', is_flag=True, help='Debug mode')
def bclean(branch, branches, debug):
    # Description
    """Used to delete useless local branch"""

    # Init logger
    logger = mylogger.get_logger()

    # Checking options
    if (not branch in branches):
        logger.warning('Excluded branches not contain the branch. We add it!')
        branches.append(branch)

    # Calling method
    return branches_clean(logger, branch, branches, debug)

@git_ext.command(context_settings={'show_default': True})
@argument('target', type=str, default=environ.get('GIT_DEFAULT_BRANCH'), required=False)
@option('-b', '--branch', type=str, help='Branch you want to rebase [default: your current branch]')
@option('--debug', is_flag=True, help='Debug mode')
def rfrom(target, branch, debug):
    # Description
    """Used rebase branch on target one"""

    # Init logger
    logger = mylogger.get_logger()

    # Calling method
    return refresh_from(logger, target, branch, debug)

@git_ext.command(context_settings={'show_default': True})
@option('-f', '--force', is_flag=True, help='Force refresh operation')
@option('-r', '--repository', type=str, help='Local `configuration` repository path', default=environ.get('CONFIG_PATH'))
@option('-d', '--directory', type=str, help='Directory with all the target repositories', default=environ.get('GIT_PATH'))
@option('--debug', is_flag=True, help='Debug mode')
def rhooks(force, repository, directory, debug):
    # Description
    """Used to replace existing repository hook scripts by the last version"""

    # Init logger
    logger = mylogger.get_logger()

    # Checking options
    if (not path.exists(repository)):
        conf_error_message = f'Configuration directory "{repository}" does not exist'
        logger.error(conf_error_message)
        exit()

    if (not path.exists(directory)):
        wd_error_message = f'Working directory "{directory}" does not exist'
        logger.error(wd_error_message)
        exit()

    # Calling method
    return refresh_hooks(logger, force, repository, directory, debug)

if __name__ == '__main__':
    git_ext()
