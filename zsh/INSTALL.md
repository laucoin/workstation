# Zsh configuration

## Prerequisites

1. Install zsh, git, curl and python:
    - Linux (Debian/Ubuntu)
        ```bash
        sudo apt install zsh git curl python3
        curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.5/install.sh | bash
        ```
    - Linux (CentOS)
        ```bash
        sudo yum install zsh git curl python3
        curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.5/install.sh | bash
        ```
    - MacOS
        if needed, install [Homebrew](https://brew.sh/)
        ```bash
        brew install zsh git curl nvm python
        ```
    > Replace `v0.39.5` with the repository [last version](https://github.com/nvm-sh/nvm/releases)

2. Install Python libraries :
    ```bash
    pip3 install click colorlog
    ```

3. Install [my logger library](https://gitlab.com/laucoin/my-logger)

> **_NOTE:_**
> Bear in mind that many scripts are based on Python 3.11. By downgrade the version, you'll probably break some scripts.

## Setup

1. Change the default shell:
    ```bash
    chsh -s $(which zsh)
    ```
2. Install oh-my-zsh:
    ```bash
    sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
    ```
3. Install [Inconsolata font](https://fonts.google.com/specimen/Inconsolata) and change your terminal default font
4. Create symbolic link of .zshrc into your home
    ```bash
    ln -s ~/Documents/Git/Perso/workstation/zsh/zshrc ~/.zshrc
    ```
    If the previous command failed, maybe is due to an existing `~/.zshrc`, If you don't have any existing config, you can force with `-f`:
    ```bash
    ln -f -s ~/Documents/Git/Perso/workstation/zsh/zshrc ~/.zshrc
    ```
5. Install zsh-autosuggestions plugin:
    ```bash
    git clone https://github.com/zsh-users/zsh-autosuggestions ~/.zsh/zsh-autosuggestions
    ```
6. Install zsh-syntax-highlighting plugin:
    ```bash
    git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
    ```
7. Add to your ~/.zprofile
    ```bash
    # Git
    export GIT_DEFAULT_BRANCH="main"
    ```
8. (optional) Update your ~/.zshrc and adapt your repositories path
    ```bash
    # Git
    export GIT_PATH="$HOME/your-custom-path"
    export CONFIG_PATH="$GIT_PATH/your-custom-path"
    ```

## Help

If you want to understand this configuration, you can check [zsh documentation](https://www.zsh.org/).

### System extension

#### Java version
##### Prerequisites

[Install](https://www.oracle.com/fr/java/technologies/downloads/) the wanted JDK version before.

##### Usage

Usage: jv [TARGET_JAVA_VERSION]

  Used to change java version

#### Port usage

Usage: uport [OPTIONS]

  Used to scan port usage

Options:
  -p, --port INTEGER  Port to scan
  --help              Show this message exit.

#### Kill port usage

Usage: kport [OPTIONS]

  Used to kill process linked to specified port

Options:
  -p, --port INTEGER  Port to kill
  --help              Show this message and exit.

### Git extension

#### Clean branches

Usage: bclean [OPTIONS]

  Used to delete useless local branch

Options:
  -d, --default TEXT   Default branch must be contained in excluded branches
                       [default: develop]
  -b, --branches TEXT  List of excluded branches (can be used multiple times)
                       [default: develop, main, master]
  --help               Show this message and exit.

#### Rebase branch from another

Usage: rfrom [OPTIONS]

  Used rebase branch on target one

Options:
  -d, --default TEXT  Target rebase branch
                      [default: develop]
  -b, --branch TEXT   Branch you want to rebase
                      [default: your current branch]
  --help              Show this message and exit.

#### Update git hooks on existing repositories

Usage: rhooks [OPTIONS]

  Used to replace existing repository hook scripts by the last version

Options:
  -f, --force            Force refresh operation
  -r, --repository TEXT  Local `configuration` repository path
                         [default: /Users/laucoin/Documents/Git/Perso/workstation]
  -d, --directory TEXT   Directory with all the target repositories
                         [default: /Users/laucoin/Documents/Git]
  --help                 Show this message and exit.
