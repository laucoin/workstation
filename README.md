# Workstation configuration

<!-- ALL-CONTRIBUTORS-BADGE:START - Do not remove or modify this section -->
[![All Contributors](https://img.shields.io/badge/all_contributors-1-orange.svg?style=flat-square)](#contributors-)
<!-- ALL-CONTRIBUTORS-BADGE:END -->

## This repository

This repository contains all configuration file for my workstation tools.

## How to install and use it ?

1. Clone this repository with the following command:
    ```bash
    git clone git@gitlab.com:laucoin/workstation.git ~/Documents/Git/Perso/workstation
    ```
    OR
    ```bash
    git clone https://gitlab.com/laucoin/workstation.git ~/Documents/Git/Perso/workstation
    ```
2. Follow instructions for the installation of your needs:
    - [ACK](ack/INSTALL.md)
    - [Git](git/INSTALL.md)
    - [Vim](vim/INSTALL.md)
    - [ZSH](zsh/INSTALL.md)

## Contributing

The `main` branch contains the stable code.

If you have more question, please have a look on [contributing file](https://gitlab.com/laucoin/global-readme/-/blob/main/CONTRIBUTING.md)

## Contributors ✨

Thanks goes to these wonderful people ([emoji key](https://allcontributors.org/docs/en/emoji-key)):

<!-- ALL-CONTRIBUTORS-LIST:START - Do not remove or modify this section -->
<!-- prettier-ignore-start -->
<!-- markdownlint-disable -->
<table>
  <tbody>
    <tr>
      <td align="center"><a href="https://luc-aucoin.fr"><img src="https://avatars.githubusercontent.com/u/31480129?v=4?s=100" width="100px;" alt="Luc AUCOIN"/><br /><sub><b>Luc AUCOIN</b></sub></a><br /><a href="https://github.com/laucoin/git-resources/commits?author=laucoin" title="Code">💻</a> <a href="https://github.com/laucoin/git-resources/commits?author=laucoin" title="Documentation">📖</a> <a href="#maintenance-laucoin" title="Maintenance">🚧</a></td>
    </tr>
  </tbody>
</table>

<!-- markdownlint-restore -->
<!-- prettier-ignore-end -->

<!-- ALL-CONTRIBUTORS-LIST:END -->

This project follows the [all-contributors](https://github.com/all-contributors/all-contributors) specification.
Contributions of any kind welcome!
