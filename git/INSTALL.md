# Git configuration

## Prerequisites

1. Install git:
    - Linux (Debian/Ubuntu)
        ```bash
        sudo apt install git
        ```
    - Linux (CentOS)
        ```bash
        sudo yum install git
        ```
    - MacOS
        if need install [Homebrew](https://brew.sh/)
        ```bash
        brew install git
        ```
2. Install [my logger library](https://gitlab.com/laucoin/my-logger)

## Setup

1. Create symbolic link of .gitconfig into your home
    ```bash
    ln -s $CONFIG_PATH/git/gitconfig ~/.gitconfig
    ```
    If the previous command failed, maybe is due to an existing `~/.gitconfig`, If you don't have any existing config, you can force with `-f`:
    ```bash
    ln -f -s $CONFIG_PATH/git/gitconfig ~/.gitconfig
    ```
2. Make hook executable
    ```bash
    chmod +x $CONFIG_PATH/git/git-templates/hooks/*
    ```
3. (optional) Update existing repositories hooks
    If you want to apply hooks to existing repositories, you have to add/replace hooks scripts into your `.git` folder. To do that you can:
    - Use my alias `rhooks` (check [zsh configuration](../zsh/INSTALL.md))
    - Copy `git-templates/hooks` content to your `.git/hooks` folder.

> **_NOTE:_** By default, the configuration and hooks will be applied to the new repositories.

## Help

If you want to understand this configuration, you can check [git documentation](https://git-scm.com/doc).

### Hooks
#### commit-msg

```mermaid
flowchart TD
    A(fa:fa-code-commit git commit) --> B{fa:fa-spinner\nChecking syntax}
    B -->|OK fa:fa-check| C(fa:fa-code-commit Finish commit)
    B -->|First KO fa:fa-xmark| D(fa:fa-code-branch Get current branch)
    D --> E(fa:fa-terminal Extract keywords\nEx : `feat/branch` for `long/name/for/feat/branch`)
    E --> F(fa:fa-message Create new commit message)
    F -->B
    B -->|Next KO fa:fa-xmark| G(fa:fa-xmark Display error message)
```

### post-checkout OR post-merge

```mermaid
flowchart TD
    A(fa:fa-code-merge git pull OR git checkout) --> B(fa:fa-arrow-down Get changed files)
    B --> C{fa:fa-spinner\nIs package.json changed?}
    C -->|YES| D(fa:fa-download Run `npm install`)
    D --> E
    C -->|NO| E(fa:fa-download End)
```