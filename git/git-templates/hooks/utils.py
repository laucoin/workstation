from subprocess import run, check_output, CalledProcessError

def _get_changed_files(logger, origin, head):
    logger.info('Getting changed files')
    command = ['git', 'diff-tree', '-r', '--name-only', '--no-commit-id', origin, head]
    return run(command, capture_output=True, text=True).stdout.splitlines()

def _update_dependencies(logger, changed_files):
    if ('package.json' in changed_files):
        logger.info('Updating Node modules')
        run(['npm', 'install'])

    # Update dependencies of other project

def update_dependencies_if_necessary(logger, origin, head):
    # Description
    """Update dependencies if config file changed"""

    changed_files = _get_changed_files(logger, origin, head)
    _update_dependencies(logger, changed_files)

def get_current_branch(logger):
    try:
        logger.info('Getting current branch')
        command = ['git', 'branch', '--show-current']
        return check_output(command).decode().strip()
    except CalledProcessError as e:
        branch_error_message = f'Failed get current branch with `{" ".join(command)}`:\n{e}'
        logger.error(branch_error_message)
        raise CalledProcessError(branch_error_message)